package com.dmytrov.yaypay.cachemap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.util.Pair;

import java.util.HashMap;
import java.util.Map;

@NoArgsConstructor
public class CacheMapImpl<KeyType, ValueType> implements CacheMap<KeyType, ValueType> {
    @Getter
    @Setter
    private long timeToLive;

    private Map<KeyType, Pair<Long, ValueType>> map = new HashMap<>();


    public CacheMapImpl( long timeToLive ) {
        this.timeToLive = timeToLive;
    }

    @Override
    public ValueType put( KeyType key, ValueType value ) {
        Pair<Long, ValueType> put = map.put( key, Pair.of( Clock.getTime(), value ) );
        if ( put == null || isExpired( put )) {
            return null;
        }
        return put.getSecond();
    }

    @Override
    public void clearExpired() {
        map.values().removeIf( this::isExpired );

    }

    private boolean isExpired( Pair<Long, ValueType> longValueTypePair ) {
        return longValueTypePair.getFirst() < Clock.getTime() - timeToLive;
    }

    private boolean isNotExpired( Pair<Long, ValueType> pair ) {
        return !isExpired( pair );
    }

    @Override
    public void clear() {
        map.clear();
    }

    @Override
    public boolean containsKey( Object key ) {
        Pair<Long, ValueType> pair = map.get( key );
        return pair != null && !isExpired( pair );
    }

    @Override
    public boolean containsValue( Object value ) {
        for ( Pair<Long, ValueType> pair : map.values() ) {
            if ( isNotExpired( pair ) && pair.getSecond().equals( value ) )
                return true;
        }
        return false;
    }

    @Override
    public ValueType get( Object key ) {
        Pair<Long, ValueType> pair = map.get( key );
        if ( pair != null && isNotExpired( pair ) ) {
            return pair.getSecond();
        }
        return null;
    }

    @Override
    public boolean isEmpty() {
        return map.values().stream().noneMatch( this::isNotExpired );
    }

     @Override
    public ValueType remove( Object key ) {
        Pair<Long, ValueType> remove = map.remove( key );
        if ( remove == null || isExpired( remove ) ) {
            return null;
        }
        return remove.getSecond();
    }

    @Override
    public int size() {
        return ( int ) map.values().stream().filter( this::isNotExpired ).count();
    }
}
