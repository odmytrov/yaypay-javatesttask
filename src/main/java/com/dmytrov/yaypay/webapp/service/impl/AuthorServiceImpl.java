package com.dmytrov.yaypay.webapp.service.impl;

import com.dmytrov.yaypay.webapp.dataclass.Author;
import com.dmytrov.yaypay.webapp.repository.AuthorRepository;
import com.dmytrov.yaypay.webapp.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class AuthorServiceImpl implements AuthorService {
    @Autowired
    private AuthorRepository authorRepository;

    @Override
    public Author getAuthor( long id ) {
        return authorRepository.getAuthorById(id);
    }

    @Override
    public Page<Author> getAuthors( Pageable page ) {
        return authorRepository.findAll( page );
    }

    @Override
    public Author saveAuthor( Author author ) {
        return authorRepository.save( author );
    }

    @Override
    public void removeAuthor( long id ) {
        authorRepository.delete( id );
    }
}
