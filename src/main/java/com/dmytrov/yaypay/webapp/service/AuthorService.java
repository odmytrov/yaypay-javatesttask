package com.dmytrov.yaypay.webapp.service;

import com.dmytrov.yaypay.webapp.dataclass.Author;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface AuthorService {
    Author getAuthor( long id );

    Page<Author> getAuthors( Pageable page );

    Author saveAuthor( Author author );

    void removeAuthor( long id );
}
