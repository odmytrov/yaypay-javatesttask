package com.dmytrov.yaypay.webapp.service;

import com.dmytrov.yaypay.webapp.dataclass.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface BookService {
    Book getBook( long id );

    Page<Book> getBooks( Pageable pageable );

    Book saveBook( Book book );

    void removeBook( long id );
}
