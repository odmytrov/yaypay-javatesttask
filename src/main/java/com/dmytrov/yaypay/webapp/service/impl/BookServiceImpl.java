package com.dmytrov.yaypay.webapp.service.impl;

import com.dmytrov.yaypay.webapp.dataclass.Book;
import com.dmytrov.yaypay.webapp.repository.BookRepository;
import com.dmytrov.yaypay.webapp.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class BookServiceImpl implements BookService {

    @Autowired
    private BookRepository bookRepository;

    @Override
    public Book getBook( long id ) {
        return bookRepository.getBookById( id );
    }

    @Override
    public Page<Book> getBooks( Pageable pageable ) {
        return bookRepository.findAll(pageable);
    }

    @Override
    public Book saveBook( Book book ) {
        return bookRepository.save( book );
    }

    @Override
    public void removeBook( long id ) {
        bookRepository.delete( id );
    }
}
