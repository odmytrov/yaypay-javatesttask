package com.dmytrov.yaypay.webapp.controller;

import com.dmytrov.yaypay.webapp.dataclass.Author;
import com.dmytrov.yaypay.webapp.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@RestController
public class AuthorController {

    @Autowired
    private AuthorService authorService;

    @GetMapping("/getAuthor")
    @ResponseBody
    @Transactional(readOnly = true)
    public Author getAuthor( @RequestParam(name = "id") long id ) {
        return this.authorService.getAuthor( id );
    }

    @GetMapping("/removeAuthor")
    @ResponseBody
    public void removeAuthor( @RequestParam(name = "id") long id ) {
        this.authorService.removeAuthor( id );
    }

    @PostMapping("/saveAuthor")
    @ResponseBody
    public Author saveAuthor( @RequestBody Author author ) {
        return this.authorService.saveAuthor( author );
    }

    @GetMapping("/getAuthors")
    @ResponseBody
    @Transactional(readOnly = true)
    public Page<Author> getAuthors( @PageableDefault Pageable page ) {
        return this.authorService.getAuthors( page );
    }
}
