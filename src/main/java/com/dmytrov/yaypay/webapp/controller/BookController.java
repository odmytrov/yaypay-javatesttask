package com.dmytrov.yaypay.webapp.controller;

import com.dmytrov.yaypay.webapp.dataclass.Book;
import com.dmytrov.yaypay.webapp.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@RestController
public class BookController {
    @Autowired
    private BookService bookService;

    @GetMapping("/getBook")
    @ResponseBody
    @Transactional(readOnly = true)
    public Book getBook( @RequestParam(name = "id") long id) {
        return this.bookService.getBook(id);
    }

    @GetMapping("/removeBook")
    @ResponseBody
    public void removeBook( @RequestParam(name = "id") long id) {
        this.bookService.removeBook(id);
    }

    @PostMapping("/saveBook")
    @ResponseBody
    public Book saveBook( @RequestBody Book book) {
        return this.bookService.saveBook(book);
    }

    @GetMapping("/getBooks")
    @ResponseBody
    @Transactional(readOnly = true)
    public Page<Book> getBooks( @PageableDefault Pageable pageable) {
        return this.bookService.getBooks(pageable);
    }
}
