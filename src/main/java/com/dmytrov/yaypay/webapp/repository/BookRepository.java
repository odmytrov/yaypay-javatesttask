package com.dmytrov.yaypay.webapp.repository;

import com.dmytrov.yaypay.webapp.dataclass.Book;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface BookRepository extends PagingAndSortingRepository<Book, Long> {
    Book getBookById( long id );
}
