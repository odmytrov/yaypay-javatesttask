package com.dmytrov.yaypay.webapp.repository;

import com.dmytrov.yaypay.webapp.dataclass.Author;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface AuthorRepository extends PagingAndSortingRepository<Author, Long> {
    Author getAuthorById( long id );
}
