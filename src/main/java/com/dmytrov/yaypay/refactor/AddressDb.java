package com.dmytrov.yaypay.refactor;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class AddressDb {
    private int nThreads = 18;
    private Executor executor = Executors.newFixedThreadPool( nThreads );
    private int cacheSize;
    private String url;
    private String user;
    private String password;

    public AddressDb() {


    }

    public void init() {
        Properties properties = Utils.parseProperties( "address/db.properties" );
        if ( properties == null ) {
            throw new RuntimeException( "db  properties not found" );
        }
        url = properties.getProperty( "url", "jdbc:oracle:thin:@prod" );
        user = properties.getProperty( "user", "admin" );
        password = properties.getProperty( "password", "beefhead" );
        cacheSize = Integer.parseInt( properties.getProperty( "cacheSize", "50" ) );

//        String className = properties.getProperty( "className", "oracle.jdbc.ThinDriver" );
//        try {
//            Class.forName( className );
//        } catch ( ClassNotFoundException e ) {
//            throw new RuntimeException( "Driver not found", e );
//        }
    }

    public void addPerson( Person person ) {
        try ( Connection connection = getConnection() ) {
            try ( PreparedStatement statement = connection.prepareStatement( "INSERT INTO AddressEntry VALUES (?, ?, ?)" ) ) {
                statement.setLong( 1, System.currentTimeMillis() );
                statement.setString( 2, person.getName() );
                statement.setString( 3, person.getPhoneNumber() );
                statement.executeUpdate();
            }
        } catch ( SQLException e ) {
            throw new RuntimeException( e );
        }
    }

    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection( url, user, password );
    }

    /**
     * Looks up the given person, null if not found.
     */
    public Person findPerson( String name ) {
        try ( Connection connection = getConnection() ) {
            try ( PreparedStatement preparedStatement = connection.prepareStatement( "SELECT * FROM AddressEntry WHERE name = ?" ) ) {
                preparedStatement.setString( 1, name );
                try ( ResultSet resultSet = preparedStatement.executeQuery() ) {
                    if ( resultSet.next() ) {
                        return getPerson( resultSet );
                    } else {
                        return null;
                    }
                }
            }
        } catch ( SQLException e ) {
            throw new RuntimeException( e );
        }
    }

    public List<Person> getAll() {
        try ( Connection connection = getConnection() ) {
            try ( PreparedStatement statement = connection.prepareStatement( "SELECT * FROM AddressEntry" ) ) {
                try ( ResultSet result = statement.executeQuery() ) {
                    result.setFetchSize( cacheSize );
                    List<Person> entries = new LinkedList<>();
                    while ( result.next() ) {
                        entries.add( getPerson( result ) );
                    }
                    return entries;
                }
            }
        } catch ( SQLException e ) {
            throw new RuntimeException( e );
        }
    }

    private Person getPerson( ResultSet result ) throws SQLException {
        String name = result.getString( "name" );
        String phoneNumber = result.getString( "phoneNumber" );
        long dateLong = result.getLong( 1 );
        return new Person( name, phoneNumber, dateLong );
    }


    public int getSize() {
        try ( Connection connection = getConnection() ) {
            try ( PreparedStatement statement = connection.prepareStatement( "SELECT count(*) FROM AddressEntry" ) ) {
                try ( ResultSet result = statement.executeQuery() ) {
                    if ( result.next() ) {
                        return result.getInt( "count" );
                    }
                }
            }
        } catch ( SQLException e ) {
            throw new RuntimeException( e );
        }
        return 0;
    }
}
