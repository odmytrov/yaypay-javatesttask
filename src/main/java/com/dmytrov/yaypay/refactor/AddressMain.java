package com.dmytrov.yaypay.refactor;

public class AddressMain {
    public static void main( String[] args ) {
        AddressDb addressDb = new AddressDb();
        addressDb.init();
        AddressBook addressBook = new AddressBook();
        addressBook.setAddressDb( addressDb );
        addressBook.init();
    }
}
