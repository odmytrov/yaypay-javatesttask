package com.dmytrov.yaypay.refactor;


import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class Person {
    private String name;
    private String phoneNumber;
    private Date date;

    public Person( String name, String phoneNumber, long date ) {
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.date = new Date( date );
    }

}
