package com.dmytrov.yaypay.refactor;

import lombok.Setter;

import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

public class AddressBook {

    private static String mobilePrefix = "070";
    @Setter
    private AddressDb addressDb;

    public boolean hasMobile( String name ) {
        return hasMobile( addressDb.findPerson( name ) );
    }

    private boolean hasMobile( Person person ) {
        return hasPhonePrefix( person, mobilePrefix );
    }

    private boolean hasPhonePrefix( Person person, String prefix ) {
        return person != null && person.getPhoneNumber().startsWith( prefix );
    }

    public int getSize() {
        return addressDb.getSize();
    }

    /**
     * Get the given user's mobile phone number,
     * or null if he doesn't have one.
     */
    public String getMobile( String name ) {
        Person person = addressDb.findPerson( name );
        if ( person != null && hasMobile(person) ) {
            return person.getPhoneNumber();
        } else {
            return null;
        }
    }

    /**
     * Returns all names in the book. Truncates to the given length.
     */
    public List<String> getNames( int maxLength ) {
        return addressDb.getAll().stream().
                map( Person::getName )
                .map( name -> name.substring( 0, maxLength ) )
                .collect( Collectors.toCollection( LinkedList::new ) );

    }

    /**
     * Returns all people who have mobile phone numbers.
     */
    public List<Person> getPersonsWithMobilePhone() {
        return addressDb.getAll().stream()
                .filter( this::hasMobile )
                .collect( Collectors.toCollection( LinkedList::new ) );
    }

    public void init() {
        Properties properties = Utils.parseProperties( "address/address.properties" );
        if ( properties == null ) {
            throw new RuntimeException( "address properties not found" );
        }
        mobilePrefix = properties.getProperty( "mobile.prefix", "070" );
    }
}
