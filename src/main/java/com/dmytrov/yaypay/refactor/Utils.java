package com.dmytrov.yaypay.refactor;

import java.io.*;
import java.util.Properties;

public class Utils {
    public static Properties parseProperties( String resourcePath ) {
        Properties properties = new Properties();
        try ( InputStream propertiesIn = ClassLoader.class.getResourceAsStream( resourcePath ) ) {
            File targetPropertiesFile = new File( resourcePath );
            if ( !targetPropertiesFile.exists() ) {
                targetPropertiesFile.getParentFile().mkdirs();
                targetPropertiesFile.createNewFile();
                try ( FileOutputStream propertiesOut = new FileOutputStream( targetPropertiesFile ) ) {
                    byte[] buffer = new byte[propertiesIn.available()];
                    propertiesIn.read( buffer );
                    propertiesOut.write( buffer );
                }
            }
            try ( InputStream inStream = new FileInputStream( targetPropertiesFile ) ) {
                properties.load( inStream );
                return properties;
            }
        } catch ( IOException e ) {
            e.printStackTrace();
        }
        return null;
    }
}
