package com.dmytrov.yaypay.refactor;

/**
 * Obvious class that wrap wrapper of phone number.
 *
 * Will be removed in next version.
 */
@Deprecated
public class PhoneNumber {
    private Num number;

    public PhoneNumber( String number ) {
        this.number = new Num( number );
    }


    /**
     * getter should not look such way
     */
    public String getNumber() {
        return number.getNumber();
    }

}
