package com.dmytrov.yaypay.refactor;

/**
 * Obvious class that wrap phone number with ugly name and fields.
 *
 * Will be removed in next version.
 */
@Deprecated()
class Num {
    private String val;

    public Num( String x ) {
        this.val = x;
    }

    public String getNumber() {
        return val;
    }

}
